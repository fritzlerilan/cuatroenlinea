#include "game_structs.h"
#include "game.h"


//	Juego de 4 en Linea. Para poner descanso al proyecto del bingo jaja.



int main(){
	srand(time(NULL));

	int fils = 6, cols = 6;

	char nombre_p1[20], nombre_p2[20];
	

	//Datos Jugadores
	Jugador player1, player2;

	int** tab_din = inic_tablero(fils, cols);
	CLEAR;
	mostrar_tabla(tab_din, fils, cols);

	printf("\nJugador 1 Pieza ");
	PRINT_CIRCLE;
	printf(" -> Cual es su nombre?: ");
	fflush(stdin);
	scanf("%s", nombre_p1);

	printf("\nJugador 2 Pieza ");
	PRINT_EQUIS;
	printf(" -> Cual es su nombre?: ");
	fflush(stdin);
	scanf("%s", nombre_p2);

	int col_elegida;
	int winner = 0;
	int turno = 0;
	int pieza;

	inic_jugador(&player1, nombre_p1, nombre_p2, 1);
	inic_jugador(&player2, nombre_p2, nombre_p1, 2);

	int valida;

	while(!winner){
		
		if(turno % 2 == 0){
			pieza = 1;
			do{
				col_elegida = pedir_col_jugador(cols, turno, player1.nombre);
				valida = colocar_en_columna(tab_din, fils, cols, col_elegida, pieza);
				if(!valida){
					printf("\nELIJA OTRA COLUMNA POR FAVOR! COLUMNA LLENA!\n");
				}
			}while(!valida);
			player1.marca = MARCA_O;
			player1.cant_tiradas++;
		}else{
			pieza = 2;
			do{
				col_elegida = pedir_col_jugador(cols, turno, player2.nombre);
				valida = colocar_en_columna(tab_din, fils, cols, col_elegida, pieza);
				if(!valida){
					printf("\nELIJA OTRA COLUMNA POR FAVOR! COLUMNA LLENA!\n");
				}
			}while(!valida);
			
			player2.marca = MARCA_X;
			player2.cant_tiradas++;
		}
		int valida = 1;

		
		winner = movimiento_ganador(tab_din, fils, cols, pieza);

		turno++;
	}

	if(winner == 1){
		printf("\nHubo ganador!\n");
	}
		
	return 0;
}