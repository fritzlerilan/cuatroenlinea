#ifndef GAME_STRUCT__
#define GAME_STRUCT__

#include <stdio.h>
#include <stdlib.h>

#define MARCA_V ' '
#define MARCA_O 'O'
#define MARCA_X 'X'

#define TAM_MIN 4

typedef struct
{
	char nombre[20];
	char nombre_rival[20];
	char marca;				// X - O

	int cant_tiradas;		// Cantidad de turnos que necesitó para ganar.

}Jugador;


#endif