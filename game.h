#ifndef GAME_FOURINLINE__
#define GAME_FOURINLINE__
#include "game_structs.h"
#include <string.h>

#ifdef _WIN32
#include <windows.h>		//COMPATIBILIDAD WIN32 WIN64

#define PRINT_CIRCLE printf("[%c]", MARCA_O)	// FALTA INCLUIR COLOR AZUL
#define PRINT_EQUIS printf("[%c]", MARCA_X)		// FALTA INCLUIR COLOR ROJO
#define SLEEP Sleep(200)
#define CLEAR system("cls")
#endif

#ifdef linux				//COMPATIBILIDAD LINUX

#define PRINT_CIRCLE printf("\e[1;44;98m[%c]\e[0m", MARCA_O)
#define PRINT_EQUIS printf("\e[1;41;98m[%c]\e[0m", MARCA_X)
#define SLEEP system("sleep 0.2s")
#define COLOR_CIRCLE printf("\e[1;44;98m")
#define COLOR_EQUIS printf("\e[1;41;98m")
#define COLOR_R printf("\e[0m")
#define CLEAR system("clear")
#endif

//	Tablero
int** inic_tablero(int filas, int columnas);
void imprimir_numeros(int columnas);
void mostrar_tabla(int** matriz, int filas, int columnas);
int colocar_en_columna(int** tablero, int f, int c, int columna, int pieza);

//	Jugador
int movimiento_ganador(int** tab, int filas, int columnas, int pieza);
int pedir_col_jugador(int columnas, int turno, const char* nombre);
void inic_jugador(Jugador* jugador, const char* nombre_jugador, const char* nombre_riv, int num_jugador);

#endif