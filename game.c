#include "game.h"

/**
 * Brief: 	Inicializa el tablero con valores vacios. 
 * P1:    	Matriz Tablero.
 *
 * R: 		Puntero doble a la matriz.
 **/

int** inic_tablero(int filas, int columnas){
	
	int** tablero = (int**) calloc(filas, sizeof(int*));
	
	for(int i = 0; i < filas; i++){
		tablero[i] = (int*) calloc(columnas, sizeof(int));
	}

	return tablero;
}

/**
 * Brief: 	Muestra una matriz
 * P1:    	Matriz Tablero.
 * P2:	  	Filas
 * P3:    	Columnas
 **/
void mostrar_tabla(int** matriz, int filas, int columnas){
	imprimir_numeros(columnas);
	for(int i = 0; i < filas; i++){
		for(int j = 0; j < columnas; j++){
			if(matriz[i][j] == 0)
			{
				printf("[%c]", MARCA_V);
			}
			else if(matriz[i][j] == 1)
			{
				PRINT_CIRCLE;
			}
			else
			{
				PRINT_EQUIS;

			}
		}
		printf("\n");
	}
	printf("\n");
}

/**
 * Brief: 	Coloca una pieza en el tablero y muestra la animacion.
 * P1:    	Matriz tablero.
 * P2:	  	Filas
 * P3:    	Columnas
 * P4:   	columna donde depositar el valor
 * P5: 		La pieza a insertar (int) #_definida en game_structs.h
 *
 * R: 		Si fue exitosa la colocacion de la pieza
 **/
int colocar_en_columna(int** tablero, int f, int c, int columna, int pieza){
	
	int i = 0;
	int valida = 0;
	while(i < f && tablero[i][columna] == 0){
		valida = 1;
		system("clear");
		
		tablero[i][columna] = pieza;

		if(i > 0){
			tablero[i-1][columna] = 0;
		}
		i++;
		mostrar_tabla(tablero, f, c);
		SLEEP;
	}
	return valida;
}
/**
 * Brief: 	Imprime los numeros en el tablero
 * P1:    	Matriz Tablero.
 * P2:	  	Filas
 * P3:    	Columnas
 **/
void imprimir_numeros(int columnas){
	for(int i = 0; i < columnas; i++){
		printf("%2d ", i+1);
	}
	printf("\n");
}


int movimiento_ganador(int** tab, int filas, int columnas, int pieza){
	int hay_ganador = 0;

	//Evaluo horizontal derecha
	for(int i = filas - 1; i >= 0; i--){		// Recorre de abajo hacia arriba
		for(int j = 0; j < columnas - 3; j++){	// desde columna 0 a n - 3
			if(tab[i][j] == pieza && tab[i][j+1] == pieza && tab[i][j+2] == pieza && tab[i][j+3] == pieza){
				hay_ganador = 1;
			}
		}
	}

	//Evaluo horizontal izquierda
	if(!hay_ganador){
		for(int i = filas - 1; i >= 0; i--){		// Recorre de abajo hacia arriba
			for(int j = columnas - 1; j > 3; j--){	// desde columna n - 3 a 0
				if(tab[i][j] == pieza && tab[i][j-1] == pieza && tab[i][j-2] == pieza && tab[i][j-3] == pieza){
					hay_ganador = 1;
				}
			}
		}
	}
	//Evaluo vertical
	if(!hay_ganador){
		for(int i = filas - 1; i > 2; i--){
			for(int j = 0; j < columnas; j++){
				if(tab[i][j] == pieza && tab[i-1][j] == pieza && tab[i-2][j] == pieza && tab[i-3][j] == pieza){
					hay_ganador = 1;
				}
			}
		}
	}
	//Evaluo diagonal hacia la derecha
	if(!hay_ganador){
		for(int i = filas - 1; i > 3; i--){
			for(int j = 0; j < columnas - 3; j++){
				if(tab[i][j] == pieza && tab[i-1][j+1] == pieza && tab[i-2][j+2] == pieza && tab[i-3][j+3] == pieza){
					hay_ganador = 1;
				}
			}
		}
	}
	//Evaluo diagonal hacia la izquierda
	if(!hay_ganador){
		for(int i = filas - 1; i > 3; i--){
			for(int j = columnas - 4; j > 3; j--){
				if(tab[i][j] == pieza && tab[i-1][j-1] == pieza && tab[i-2][j-2] == pieza && tab[i-3][j-3] == pieza){
					hay_ganador = 1;
				}
			}
		}
	}


	return hay_ganador;
}

/**
 * Brief: 	Pide la columna donde tirar la ficha al jugador.
 * P1:    	Cant columnas validas.
 * P2:	  	Turno.
 * P3:    	Nombre Jugador.
 * R:     	El subindice de la columna elegida.
 **/
int pedir_col_jugador(int columnas, int turno, const char* nombre){
	int columna;
	do{
		printf("\n\nINGRESE LA COLUMNA POR LA QUE DESEA TIRAR LA FICHA ");
		#ifdef linux 
			if(turno % 2 == 0){COLOR_CIRCLE;}
				else{COLOR_EQUIS;}
			printf("%s", nombre);
			COLOR_R;
		#endif
		#ifdef _WIN32
			printf("%s", nombre);
		#endif
		printf(": ");
		fflush(stdin);
		scanf("%d", &columna);
	}while(columna < 1 || columna > columnas);

	return columna - 1;
}

/**
 * Brief: 	Inicializa los datos del jugador.
 * P1:    	Puntero a Jugador.
 * P2:	  	Nombre.
 * P3:    	Nombre del contrincante.
 * P4:		Jugador 1 o 2.
 **/
void inic_jugador(Jugador* jugador, const char* nombre_jugador, const char* nombre_riv, int num_jugador){
	strcpy(jugador->nombre, nombre_jugador);
	strcpy(jugador->nombre_rival, nombre_riv);
	jugador->cant_tiradas = 0;
	if(num_jugador == 1){
		jugador->marca = MARCA_O;
	}else{
		jugador->marca = MARCA_X;
	}
}